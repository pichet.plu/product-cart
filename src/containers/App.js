import React, { Component } from 'react';
import { Products, Cart } from '../components'
import { Button, CardImg } from 'reactstrap'
const liff = window.liff;
class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      displayName: '',
      userId: '',
      pictureUrl: '',
      statusMessage: '',
      products: [
        { id: 1, name: 'ข้าวกระเพราไก่', amount: 10, buy: 0 },
        { id: 2, name: 'แกงป่าใส่ไก่', amount: 9, buy: 0 },
        { id: 3, name: 'ต้มยำกุ้ง', amount: 8, buy: 0 },
        { id: 4, name: 'แกงเขียวหวาน', amount: 7, buy: 0 },
        { id: 5, name: 'ตำไทย', amount: 6, buy: 0 }
      ]
    }
    this.initialize = this.initialize.bind(this);
    this.closeApp = this.closeApp.bind(this);
  }
  componentDidMount() {
    window.addEventListener('load', this.initialize);
  }

  initialize() {
    liff.init(async (data) => {
      let profile = await liff.getProfile();
      this.setState({
        displayName: profile.displayName,
        userId: profile.userId,
        pictureUrl: profile.pictureUrl,
        statusMessage: profile.statusMessage
      });
    });
  }
  closeApp(event) {
    event.preventDefault();
    liff.sendMessages([{
      type: 'text',
      text: "Thank you, Bye!"
    }]).then(() => {
      liff.closeWindow();
    });
  }
  changeBuy = (id, amount) => {
    this.setState(prevState => {
      const { products } = prevState
      const index = products.findIndex(product => product.id === id)
      const product = products[index]

      return {
        products: [
          ...products.slice(0, index),
          { ...product, buy: product.buy + amount },
          ...products.slice(index + 1)
        ]
      }
    })
    // this.setState({
    //   products: [
    //     ...products.slice(0, index),
    //     { ...product, buy: product.buy + amount },
    //     ...products.slice(index + 1)
    //   ]
    // })
  }

  getAvailableProducts = () => {
    return this.state.products.filter(({ amount, buy }) => amount !== buy)
  }

  getProductsIncart = () => {
    return this.state.products.filter(({ buy }) => buy > 0)
  }
  reduceBuy = () => id => this.changeBuy(id, -1)

  increaseBuy = () => id => this.changeBuy(id, +1)

  render() {
    return (
      <div className="container">
        <div style={{ alignContent: 'center' }}>
          <Button outline color="danger" onClick={this.closeApp}>X</Button>
        </div>
        <div style={{ textAlign: 'center', marginTop: 20, marginBottom: 10 }}>
          PKINDEV SHOP
        </div>
        <CardImg bottom width="10%" src={this.state.pictureUrl} alt="Card image cap" />

        <div>displayName : {this.state.displayName}</div>
        {/* <div>userId : {this.state.userId}</div> */}
        {/* <div>pictureUrl : {this.state.pictureUrl}</div> */}
        {/* <div>statusMessage : {this.state.statusMessage}</div> */}
        <Products
          products={this.getAvailableProducts()}
          onAddToCart={this.increaseBuy()} />
        <Cart
          products={this.getProductsIncart()}
          onRemove={this.reduceBuy()} />

      </div>
    );
  }
}

export default App;
